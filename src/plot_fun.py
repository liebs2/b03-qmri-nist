import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

def plot_header():
    im2 = np.asarray(Image.open("figures/crc1456.png"))
    im1 = np.asarray(Image.open("figures/UMG_logo.png"))
    im0 = np.asarray(Image.open("figures/graz_logo.png"))
    fig, axs = plt.subplots(nrows=1,ncols=3, width_ratios=[1,2,1], figsize=(10,1))
    axs[0].axis("off")
    axs[1].axis("off")
    axs[2].axis("off")
    axs[0].imshow(im0)
    axs[1].imshow(im1)
    axs[2].imshow(im2)

def plot_qmaps():
    im0 = np.asarray(Image.open("figures/t1map.png"))
    im1 = np.asarray(Image.open("figures/t2map.png"))
    fig, axs = plt.subplots(nrows=1,ncols=2, width_ratios=[1,1], figsize=(15,15))
    axs[0].axis("off")
    axs[1].axis("off")
    axs[0].imshow(im0)
    axs[1].imshow(im1)

def plot_b0map():
    im0 = np.asarray(Image.open("figures/b0map.png"))
    fig, axs = plt.subplots(nrows=1,ncols=1,figsize=(3,3))
    axs.axis("off")
    axs.imshow(im0)

def plot_b1map():
    im0 = np.asarray(Image.open("figures/b1map.png"))
    fig, axs = plt.subplots(nrows=1,ncols=1,figsize=(3,3))
    axs.axis("off")
    axs.imshow(im0)

def plot_all_maps():
    im0 = np.asarray(Image.open("figures/b0map.png"))
    im1 = np.asarray(Image.open("figures/b1map.png"))
    im2 = np.asarray(Image.open("figures/t1map.png"))
    im3 = np.asarray(Image.open("figures/t2map.png"))
    fig, axs = plt.subplots(nrows=1,ncols=4, width_ratios=[1,1,1.75,1.75], figsize=(15,15))
    axs[0].axis("off")
    axs[1].axis("off")
    axs[2].axis("off")
    axs[3].axis("off")
    axs[0].imshow(im0)
    axs[1].imshow(im1)
    axs[2].imshow(im2)
    axs[3].imshow(im3)
